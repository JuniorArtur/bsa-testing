package com.example.demo.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.service.ToDoService;

@RestController
@RequestMapping("/todos")
public class ToDoController {

	private final ToDoService toDoService;

	@Autowired
	public ToDoController(ToDoService toDoService) {
		this.toDoService = toDoService;
	}

	@ExceptionHandler({ ToDoNotFoundException.class })
	public String handleException(Exception ex) {
		return ex.getMessage();
	}

	@GetMapping
	public @Valid List<ToDoResponse> getAll() {
		return toDoService.getAll();
	}

	@GetMapping("/find")
	public @Valid ResponseEntity<List<ToDoResponse>> getByIds(@RequestParam List<Long> ids) {
		if (ids.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		try {
			return new ResponseEntity<>(toDoService.getByIds(ids), HttpStatus.OK);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping
	public @Valid ResponseEntity<ToDoResponse> save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) {
		try {
			return new ResponseEntity<>(toDoService.upsert(todoSaveRequest), HttpStatus.OK);
		} catch (ToDoNotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/{id}/complete")
	public @Valid ToDoResponse save(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.completeToDo(id);
	}

	@GetMapping("/{id}")
	public @Valid ResponseEntity<ToDoResponse> getOne(@PathVariable Long id) {
		try {
			return new ResponseEntity<>(toDoService.getOne(id), HttpStatus.OK);
		} catch (ToDoNotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
		try {
			toDoService.deleteOne(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (ToDoNotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
