package com.example.demo.controller;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerWithRepositoryTest {

    private final MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Autowired
    ToDoControllerWithRepositoryTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void whenGetByIds_thenReturnValidResponse() throws Exception {
        final var testIds = List.of(1L, 2L);
        final var testTexts = List.of("test1", "test2");
        final var todos = new ArrayList<ToDoEntity>();
        todos.add(new ToDoEntity(testIds.get(0), testTexts.get(0)));
        todos.add(new ToDoEntity(testIds.get(1), testTexts.get(1)));

        when(this.toDoService.getByIds(testIds)).thenReturn(ToDoEntityToResponseMapper.map(todos));

        final var idsRequestParam = testIds
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        this.mockMvc
                .perform(get("/todos/find").param("ids", idsRequestParam))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testIds.get(0)))
                .andExpect(jsonPath("$[0].text").value(testTexts.get(0)))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist())
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(testIds.get(1)))
                .andExpect(jsonPath("$[1].text").value(testTexts.get(1)))
                .andExpect(jsonPath("$[1].completedAt").doesNotExist());
    }

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        final var testText = "My to do text";
        final var testId = 1L;
        when(this.toDoService.getAll()).thenReturn(
                Collections.singletonList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
                )
        );
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

}
