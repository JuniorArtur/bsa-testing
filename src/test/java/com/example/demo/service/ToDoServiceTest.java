package com.example.demo.service;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        this.toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenGetAll_thenReturnAll() {
        final var testToDos = new ArrayList<ToDoEntity>();
        testToDos.add(new ToDoEntity(0L, "Test 1"));
        final var toDo = new ToDoEntity(1L, "Test 2");
        toDo.completeNow();
        testToDos.add(toDo);
        when(toDoRepository.findAll()).thenReturn(testToDos);

        final var todos = toDoService.getAll();

        assertEquals(todos.size(), testToDos.size());
        for (int i = 0; i < todos.size(); i++) {
            assertThat(todos.get(i), samePropertyValuesAs(
                    ToDoEntityToResponseMapper.map(testToDos.get(i))
            ));
        }
    }

    @Test
    void whenUpsertWithId_thenReturnUpdated() throws ToDoNotFoundException {
        final var expectedToDo = new ToDoEntity(0L, "New Item");
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            Long id = i.getArgument(0, Long.class);
            if (id.equals(expectedToDo.getId())) {
                return Optional.of(expectedToDo);
            } else {
                return Optional.empty();
            }
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            final var arg = i.getArgument(0, ToDoEntity.class);
            final var id = arg.getId();
            if (id != null) {
                if (!id.equals(expectedToDo.getId()))
                    return new ToDoEntity(id, arg.getText());
                expectedToDo.setText(arg.getText());
                return expectedToDo;
            } else {
                return new ToDoEntity(40158L, arg.getText());
            }
        });

        final var toDoSaveRequest = new ToDoSaveRequest();
        toDoSaveRequest.id = expectedToDo.getId();
        toDoSaveRequest.text = "Updated Item";
        final var todo = toDoService.upsert(toDoSaveRequest);

        assertSame(todo.id, toDoSaveRequest.id);
        assertEquals(todo.text, toDoSaveRequest.text);
    }

    @Test
    void whenUpsertNoId_thenReturnNew() throws ToDoNotFoundException {
        final var newId = 0L;
        when(toDoRepository.findById(anyLong())).thenAnswer(i -> {
            Long id = i.getArgument(0, Long.class);
            if (id == newId) {
                return Optional.empty();
            } else {
                return Optional.of(new ToDoEntity(newId, "Wrong ToDo"));
            }
        });
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            final var arg = i.getArgument(0, ToDoEntity.class);
            final var id = arg.getId();
            if (id == null)
                return new ToDoEntity(newId, arg.getText());
            else
                return new ToDoEntity();
        });

        final var toDoDto = new ToDoSaveRequest();
        toDoDto.text = "Created Item";
        final var result = toDoService.upsert(toDoDto);

        assertEquals((long) result.id, newId);
        assertEquals(result.text, toDoDto.text);
    }

    @Test
    void whenComplete_thenReturnWithCompletedAt() throws ToDoNotFoundException {
        final var startTime = ZonedDateTime.now(ZoneOffset.UTC);
        final var todo = new ToDoEntity(0L, "Test 1");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            final var arg = i.getArgument(0, ToDoEntity.class);
            final var id = arg.getId();
            if (id.equals(todo.getId())) {
                return todo;
            } else {
                return new ToDoEntity();
            }
        });
        final var result = toDoService.completeToDo(todo.getId());

        assertEquals(result.id, todo.getId());
        assertEquals(result.text, todo.getText());
        assertTrue(result.completedAt.isAfter(startTime));
    }

    @Test
    void whenGetOne_thenReturnCorrectOne() throws ToDoNotFoundException {
        final var todo = new ToDoEntity(0L, "Test 1");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

        final var result = toDoService.getOne(0L);

        assertThat(result, samePropertyValuesAs(
                ToDoEntityToResponseMapper.map(todo)
        ));
    }

    @Test
    void whenDeleteOne_thenRepositoryDeleteCalled() {
        try {
            final var id = 0L;
            toDoService.deleteOne(id);
            verify(toDoRepository, times(1)).deleteById(id);
        } catch (ToDoNotFoundException ignored) {
        }
    }

    @Test
    void whenIdNotFound_thenThrowNotFoundException() {
        assertThrows(ToDoNotFoundException.class, () -> toDoService.getOne(1L));
    }

    @Test
    void whenGetByIds_thenReturnByIds() {
        final var testToDos = List.of(
                new ToDoEntity(0L, "Test 1"),
                new ToDoEntity(1L, "Test 2").completeNow()
        );
        final var ids = testToDos
                .stream()
                .map(ToDoEntity::getId)
                .collect(Collectors.toList());

        when(toDoRepository.findByIdIn(ids)).thenReturn(testToDos);

        try {
            final var foundToDos = toDoService.getByIds(ids);
            assertEquals(foundToDos.size(), testToDos.size());
            for (int i = 0; i < foundToDos.size(); i++) {
                assertThat(foundToDos.get(i), samePropertyValuesAs(
                        ToDoEntityToResponseMapper.map(testToDos.get(i))
                ));
            }
        } catch (NotFoundException ignored) {
        }
    }

    @Test
    void whenIdsIsEmpty_thenReturnEmptyList() {
        try {
            final var emptyIdsList = new ArrayList<Long>();
            final var result = toDoService.getByIds(emptyIdsList);
            assertEquals(Collections.emptyList(), result);
        } catch (NotFoundException ignored) {
        }
    }

    @Test
    void whenIdsNotFound_thenThrowNotFoundException() {
        final var invalidIds = List.of(-2L, -1L);
        assertThrows(NotFoundException.class, () -> toDoService.getByIds(invalidIds));
    }

}
