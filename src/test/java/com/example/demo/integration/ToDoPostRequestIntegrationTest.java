package com.example.demo.integration;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class ToDoPostRequestIntegrationTest {

    private final MockMvc mockMvc;

    private final ToDoRepository toDoRepository;

    private final ObjectMapper objectMapper;

    @Autowired
    public ToDoPostRequestIntegrationTest(MockMvc mockMvc, ToDoRepository toDoRepository) {
        this.mockMvc = mockMvc;
        this.toDoRepository = toDoRepository;
        this.objectMapper = new ObjectMapper();
    }

    @Test
    void whenSendNullableUpsertToDoRequest_thenReturnBadRequestStatus() throws Exception {
        final Long testId = null;
        final var request = new ToDoSaveRequest(testId, null);

        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(request))
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").doesNotExist());

        assertFalse(toDoRepository.existsById(testId));
    }

    @Test
    void whenSendUpsertToDoRequestWithNoText_thenReturnBadRequestStatus() throws Exception {
        final var testId = 1L;
        final var request = new ToDoSaveRequest(testId, null);

        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(request))
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest());

        assertFalse(toDoRepository.existsById(testId));
    }

    @Test
    void whenSendUpsertToDoRequestWithInvalidId_thenReturnNotFoundStatus() throws Exception {
        final var testId = -1L;
        final var request = new ToDoSaveRequest(testId, "text");

        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(request))
                .characterEncoding("utf-8"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

        assertFalse(toDoRepository.existsById(testId));
    }

    @Test
    void whenSendUpsertToDoRequestWithNoId_thenReturnToDoWithGeneretedId() throws Exception {
        final Long testId = null;
        final var testText = "text";
        final var request = new ToDoSaveRequest(testId, testText);

        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(request))
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        assertFalse(toDoRepository.existsById(testId));
    }

    @Test
    void whenSendValidUpsertToDoRequest_thenReturnOkStatusWithToDo() throws Exception {
        final var testId = 1L;
        final var testText = "text";
        final var request = new ToDoSaveRequest(testId, testText);

        this.mockMvc.perform(post("/todos")
                .content(this.objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testId))
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        Assertions.assertEquals(
                new ToDoEntity(testId, testText, null),
                toDoRepository.findById(testId).orElseThrow()
        );
    }

}
