package com.example.demo.integration;

import com.example.demo.controller.ToDoController;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIntegrationTest {

    private final MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Autowired
    ToDoControllerWithServiceIntegrationTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    void whenGetByInvalidId_thenThrowNotFoundExceptionAndReturnNotFoundStatus() throws Exception {
        final var invalidId = -1L;

        when(toDoRepository.getOne(invalidId)).thenThrow(EntityNotFoundException.class);

        this.mockMvc
                .perform(get("/todos/ " + invalidId))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void whenGetByEmptyIdsList_thenReturnBadRequestStatus() throws Exception {
        when(toDoRepository.findByIdIn(Collections.emptyList())).thenReturn(Collections.emptyList());
        this.mockMvc
                .perform(get("/todos/find").param("ids", ""))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void whenGetByInvalidIds_thenReturnNotFoundStatus() throws Exception {
        final var invalidIds = List.of(-2L, -1L);
        final var idsRequestParam = invalidIds
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        when(toDoRepository.findByIdIn(invalidIds)).thenReturn(Collections.emptyList());
        this.mockMvc
                .perform(get("/todos/find").param("ids", idsRequestParam))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

}
