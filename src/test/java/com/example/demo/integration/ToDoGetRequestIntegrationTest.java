package com.example.demo.integration;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class ToDoGetRequestIntegrationTest {

    private final MockMvc mockMvc;

    private final ToDoRepository toDoRepository;

    @Autowired
    public ToDoGetRequestIntegrationTest(MockMvc mockMvc, ToDoRepository toDoRepository) {
        this.mockMvc = mockMvc;
        this.toDoRepository = toDoRepository;
    }

    @Test
    void whenGetToDoByInvalidId_thenReturnNotFoundStatus() throws Exception {
        final var invalidId = -1L;

        this.mockMvc
                .perform(get("/todos/" + invalidId))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

        assertFalse(toDoRepository.existsById(invalidId));
    }

    @Test
    void whenGetToDoByValidId_thenReturnOkStatusAndToDoEntity() throws Exception {
        final var testId = 1L;
        final var testText = "text";
        final var toDoEntity = new ToDoEntity(testId, testText);
        toDoRepository.save(toDoEntity);

        this.mockMvc
                .perform(get("/todos/" + testId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(testId))
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.completedAt").doesNotExist());

        final var actual = toDoRepository.findById(testId).orElseThrow();
        assertThat(actual, samePropertyValuesAs(toDoEntity));
    }

    @Test
    void whenGetToDosByEmptyIdsList_thenReturnBadRequestStatus() throws Exception {
        final var ids = new ArrayList<Long>(0);

        this.mockMvc
                .perform(get("/todos/find").param("ids", ""))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").doesNotExist());

        assertFalse(toDoRepository.existsByIdIn(ids));
    }

    @Test
    void whenGetToDosByInvalidIds_thenReturnNotFoundStatus() throws Exception {
        final var invalidIds = List.of(-2L, -1L);
        final var idsRequestParam = invalidIds
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        this.mockMvc
                .perform(get("/todos/find").param("ids", idsRequestParam))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

        assertFalse(toDoRepository.existsByIdIn(invalidIds));
    }

    @Test
    void whenGetToDosByValidIds_thenReturnOkStatusAndToDoEntities() throws Exception {
        final var testIds = List.of(1L, 2L);
        final var testTexts = List.of("test1", "test2");
        final var todos = new ArrayList<ToDoEntity>();
        todos.add(new ToDoEntity(testIds.get(0), testTexts.get(0)));
        todos.add(new ToDoEntity(testIds.get(1), testTexts.get(1)));

        toDoRepository.saveAll(todos);

        final var idsRequestParam = testIds
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        this.mockMvc
                .perform(get("/todos/find").param("ids", idsRequestParam))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testIds.get(0)))
                .andExpect(jsonPath("$[0].text").value(testTexts.get(0)))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(testIds.get(1)))
                .andExpect(jsonPath("$[1].text").value(testTexts.get(1)));

        final var actual = toDoRepository.findByIdIn(testIds);
        assertThat(actual, samePropertyValuesAs(todos));
    }
}
